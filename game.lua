-- get main table for functions that we will send to main
local game = {}
game.loaded = false
-- create our base player
local player = {
  x=0,
  y=0,
  -- image in raw text location
  image="tux.png",
  speed = 500,
  fall=0,
  zivoty=5,
  body=0,
  key={}}

-- load map functions
local map = require("core.map")
-- and again our main functions
local functions = require("core.functions")

-- this variables will be used to determine where we are looking
-- really important for a lot of games
local tx = 0
local ty = 0
-- screen width and height
-- for now we set it as 0
-- but we need to declare it
local width = 0
local height = 0
-- how big force will be used to pull us down
local gravity = 25
-- id of map that we use
local map_id = 1

-- set default font
-- first parameter can be fond location
local default_font = love.graphics.newFont(30)
-- create coin from image
local coin = love.graphics.newImage("images/hud/coin.png")

-- this function will be called only once
function game.load()
  -- from image name create image in game
  player.image = love.graphics.newImage("images/player/"..player.image)

  -- if we have saved state of points read it
  if love.filesystem.isFile("body") then
    player.body = love.filesystem.read("body")
  end

  -- load actual map
  test = map.load(map_id)

  -- set our bg to image
  bg = love.graphics.newImage("maps/bg.png")
  -- set repetion of image in case that we can not fit whole screen
  bg_quad = love.graphics.newQuad(0, 0, width, height, bg:getWidth(), bg:getHeight())

  -- get id of start point
  local id = map.get_id(test, "type", "start")
  -- now get x and y of this type (we have it only once per map)
  start = map.find_id(test,id)

  -- set players x and y
  player.x = (start.x-0.5)*test.tilewidth
  player.y = (start.y-0.5)*test.tileheight
end

function game.keypressed(key, scancode, isrepeat)
  -- if we press space and we are standing on ground send us in air
  if scancode == "space" and player.fall == 0 then
    player.fall = -gravity/2
  end
end

function game.update(dt)
  if not game.loaded then
    game.load()
  end
  -- set that we can go right and left
  -- will will disable this later if needed
  local left = true
  local right = true
  -- create tempoary variable for widht and height
  tmp_width = width
  tmp_height = height
  -- get current window width and height
  width = love.graphics.getWidth()
  height = love.graphics.getHeight()
  -- if we resized window fill it with bg
  if tmp_height ~= height or tmp_width ~= width then
    bg_quad = love.graphics.newQuad(0, 0, width, height, bg:getWidth(), bg:getHeight())
  end

  -- get check box for player
  check = map.get_checkbox(test,player.x,player.y)

  -- if we are out of map send us bact to start
  -- we could add lives here
  if check.down.id == nil and check.up.id == nil and player.y > 0 then
    player.x = (start.x-0.5)*test.tilewidth
    player.y = (start.y-0.5)*test.tileheight
    player.fall = 0
    return
  end

  --if check.down.id == nil and check.up.id ~= nil then
  --  player.x = (start.x-0.5)*test.tilewidth
  --  player.y = (start.y-0.5)*test.tileheight
  --  player.fall = 0
  --  return
  --end

  -- change speed of falling based on gravity and delta time
  -- this could be done better but for game like for us it is good enough
  player.fall = player.fall + gravity * dt

  -- if we have something under player
  if check.down.test ~= nil then
    -- if it is ground then stop falling
    -- we could implement here something that would slow us down or other things
    if check.down.test.properties["type"] == "ground" and player.fall >= 0 then
      player.y = (check.center.y-0.5)*test.tilewidth
      player.fall = 0
    end
  end

  -- now check for left side
  if check.left.test ~= nil then
    -- set that we can not move left if there is ground
    if check.left.test.properties["type"] == "ground" then
      left = false
    end
    -- or if it is lock and we do not have a key
    if check.left.test.properties["type"] == "lock" and
       player.key[check.left.test.properties["color"]] == nil then
        left = false
    end
  end

  -- same thing for right side
  if check.right.test ~= nil then
    if check.right.test.properties["type"] == "ground" then
      right = false
    end
    if check.right.test.properties["type"] == "lock" and
       player.key[check.right.test.properties["color"]] == nil then
        right = false
    end
  end

  -- NOTICE: sometimes we would like to bounce
  -- up check
  if check.up.test ~= nil then
    -- if we would like to "hang" then we could place it here
    if check.up.test.properties["type"] == "ground" and player.fall < 0 then
      player.fall = 0
      player.y = (check.center.y-0.5)*test.tilewidth
    end
  end

  -- check center this is really important one for interaction and colecting things
  if check.center.test ~= nil then
    -- if we have coin here get it
    if check.center.test.properties["type"] == "coin" then
      player.body = player.body + 1
      map.set(check.center.x,check.center.y,test,1,0)
    end
    -- same for key
    if check.center.test.properties["type"] == "key" then
      player.key[check.center.test.properties["color"]] = 1
      map.set(check.center.x,check.center.y,test,1,0)
    end
    -- but if it is end send us to next map
    if check.center.test.properties["type"] == "end" then
      if not love.filesystem.isFile("body") then
        love.filesystem.newFile("body")
      end
      love.filesystem.write("body",player.body)

      map_id = map_id + 1
      -- we could do it without this part
      -- but do you know how?
      player.image = "tux.png"
      game.load()
    end
    -- if we are at ladder enable that we can go up or down
    -- without gravity
    if check.center.test.properties["type"] == "ladder" then
      if love.keyboard.isDown("w") then
        player.y = player.y - player.speed*dt
        player.fall = 0
      end
      if love.keyboard.isDown("s") then
        player.y = player.y + player.speed*dt
        player.fall = 0
      end
    end
    -- if there is a launcher use gravity and send us up
    if check.center.test.properties["type"] == "launcher" then
      player.fall = -gravity*0.75
    end
  end

  -- increment player y position by speed of falling
  player.y = player.y + player.fall

  -- if we press A and we can go left go there
  if love.keyboard.isDown("a") and left then
    player.x = player.x - player.speed*dt
  end

  -- and same for right
  if love.keyboard.isDown("d") and right then
    player.x = player.x + player.speed*dt
  end

  -- update screen position to a player
  -- NOTICE: if you see images glitching
  -- it could be because x and y position is not whole number
  -- because of that we use round on it
  tx = functions.round(player.x-width/2)
  ty = functions.round(player.y-height/2)
end

function game.draw()
  -- if game is not loaded do not try to draw
  if not game.loaded then
    return
  end
  -- first draw our background
  love.graphics.draw(bg,bg_quad,0,0)

  -- then we can draw map
  map.draw(test,tx,ty,1)

  -- now it is time for player
  -- we draw him in center since screen is moving with player
  -- if we want to move anywhere we need to use diferent approach
  love.graphics.draw(player.image,width/2-(player.image:getWidth()/2),height/2-(player.image:getHeight()/2))

  -- set font before writing anything
  -- NOTICE: it is better to send font every time
  -- it will save us problems if other parts will use diferent font
  love.graphics.setFont(default_font)
  -- set color to black (R,.G,B) max values are  255
  love.graphics.setColor(0,0,0)

  -- print how many points we have at top-right
  love.graphics.print(player.body, width-100, 10)

  -- set color to white
  -- NOTICE: you can define colors else where
  -- for example like this
  -- red = {255,0,0}
  -- and then call without any problems
  --   love.graphics.setColor(red)
  love.graphics.setColor(255,255,255)

  -- now draw coin
  -- NOTICE:x if you have diferent that white color it will
  -- add that color to image
  love.graphics.draw(coin,width-50,10)
end

-- now return game table back to main
return game
