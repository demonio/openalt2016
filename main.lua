-- require our functions for more easy use
-- I require them in every file even when I do not need them
-- this could be removed since we are not using it
local functions = require("core.functions")
-- default instance
instance = "menu"

-- all available instances
local gameinstance = {
  menu = require("menu"),
  game = require("game")
}

-- main love function for loading game parts
-- we can put here for example network init and other parts
function love.load()
  -- for each instance set that it is not loaded yet
  for key, value in pairs(gameinstance) do
    value.loaded = false
  end
end

-- main function for handling keyboard inputs
function love.keypressed(key, scancode, isrepeat)
  -- if instance has function keypressed send parameters to it
  if gameinstance[instance].keypressed then gameinstance[instance].keypressed(key, scancode, isrepeat) end
end

-- main update function dt = delta time
function love.update(dt)
  -- check if instance was loaded
  if not gameinstance[instance].loaded then
    -- if you have load function use it
    if gameinstance[instance].load then gameinstance[instance].load() end
    -- now we can set that we already loaded
    -- this could be return code of load function
    gameinstance[instance].loaded = true
  end
  -- if we have update function use it and give it dt
  if gameinstance[instance].update then gameinstance[instance].update(dt) end
end

-- main draw function this is where all draw parts will take place
function love.draw()
  -- and again if you can draw do it
  if gameinstance[instance].draw then gameinstance[instance].draw() end
end
