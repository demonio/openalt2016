return {
  version = "1.1",
  luaversion = "5.1",
  tiledversion = "0.14.2",
  orientation = "orthogonal",
  renderorder = "right-down",
  width = 25,
  height = 24,
  tilewidth = 70,
  tileheight = 70,
  nextobjectid = 1,
  properties = {},
  tilesets = {
    {
      name = "dlazdice",
      firstgid = 1,
      tilewidth = 70,
      tileheight = 70,
      spacing = 0,
      margin = 0,
      image = "images.png",
      imagewidth = 630,
      imageheight = 630,
      tileoffset = {
        x = 0,
        y = 0
      },
      properties = {},
      terrains = {},
      tilecount = 81,
      tiles = {
        {
          id = 0,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 1,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 3,
          properties = {
            ["type"] = "coin_box"
          }
        },
        {
          id = 7,
          properties = {
            ["type"] = "event"
          }
        },
        {
          id = 14,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 15,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 19,
          properties = {
            ["color"] = "blue",
            ["type"] = "base"
          }
        },
        {
          id = 20,
          properties = {
            ["color"] = "green",
            ["type"] = "base"
          }
        },
        {
          id = 22,
          properties = {
            ["color"] = "red",
            ["type"] = "base"
          }
        },
        {
          id = 24,
          properties = {
            ["color"] = "yellow",
            ["type"] = "base"
          }
        },
        {
          id = 28,
          properties = {
            ["type"] = "coin"
          }
        },
        {
          id = 29,
          properties = {
            ["type"] = "start"
          }
        },
        {
          id = 30,
          properties = {
            ["type"] = "start"
          }
        },
        {
          id = 31,
          properties = {
            ["type"] = "end"
          }
        },
        {
          id = 32,
          properties = {
            ["type"] = "end"
          }
        },
        {
          id = 35,
          properties = {
            ["color"] = "blue",
            ["type"] = "gem"
          }
        },
        {
          id = 36,
          properties = {
            ["color"] = "green",
            ["type"] = "gem"
          }
        },
        {
          id = 37,
          properties = {
            ["color"] = "red",
            ["type"] = "gem"
          }
        },
        {
          id = 38,
          properties = {
            ["color"] = "yellow",
            ["type"] = "gem"
          }
        },
        {
          id = 39,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 40,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 41,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 42,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 43,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 44,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 45,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 46,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 47,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 48,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 49,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 50,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 51,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 52,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 53,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 54,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 55,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 56,
          properties = {
            ["type"] = "ground"
          }
        },
        {
          id = 57,
          properties = {
            ["color"] = "blue",
            ["type"] = "key"
          }
        },
        {
          id = 58,
          properties = {
            ["color"] = "green",
            ["type"] = "key"
          }
        },
        {
          id = 59,
          properties = {
            ["color"] = "orange",
            ["type"] = "key"
          }
        },
        {
          id = 60,
          properties = {
            ["color"] = "yellow",
            ["type"] = "key"
          }
        },
        {
          id = 61,
          properties = {
            ["type"] = "ladder"
          }
        },
        {
          id = 62,
          properties = {
            ["type"] = "ladder"
          }
        },
        {
          id = 63,
          properties = {
            ["type"] = "water"
          }
        },
        {
          id = 64,
          properties = {
            ["type"] = "water"
          }
        },
        {
          id = 65,
          properties = {
            ["type"] = "water"
          }
        },
        {
          id = 66,
          properties = {
            ["color"] = "blue",
            ["type"] = "lock"
          }
        },
        {
          id = 67,
          properties = {
            ["color"] = "green",
            ["type"] = "lock"
          }
        },
        {
          id = 68,
          properties = {
            ["color"] = "orange",
            ["type"] = "lock"
          }
        },
        {
          id = 69,
          properties = {
            ["color"] = "yellow",
            ["type"] = "lock"
          }
        },
        {
          id = 74,
          properties = {
            ["type"] = "spike"
          }
        },
        {
          id = 75,
          properties = {
            ["type"] = "launcher"
          }
        },
        {
          id = 76,
          properties = {
            ["type"] = "launcher"
          }
        },
        {
          id = 77,
          properties = {
            ["type"] = "spawn"
          }
        },
        {
          id = 78,
          properties = {
            ["hodnota"] = "-1",
            ["type"] = "paka"
          }
        },
        {
          id = 79,
          properties = {
            ["hodnota"] = "0",
            ["type"] = "paka"
          }
        },
        {
          id = 80,
          properties = {
            ["hodnota"] = "1",
            ["type"] = "paka"
          }
        }
      }
    }
  },
  layers = {
    {
      type = "tilelayer",
      name = "LEVEL 1",
      x = 0,
      y = 0,
      width = 25,
      height = 24,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 55, 56, 56, 57, 0, 0, 0, 0,
        0, 0, 0, 31, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 41, 0, 33, 41, 0, 0, 0, 0,
        0, 0, 0, 30, 0, 0, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 67, 29, 32, 41, 0, 0, 29, 0,
        0, 43, 56, 56, 56, 56, 45, 0, 0, 0, 0, 0, 0, 0, 0, 43, 56, 52, 41, 41, 41, 56, 56, 45, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 29, 0, 0, 0, 43, 56, 56, 56, 45, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 59, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 43, 56, 56, 45, 0, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 55, 56, 57, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 29, 0, 0, 0, 0, 0, 29, 0, 68, 58, 41, 57, 0, 29, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 44, 56, 56, 56, 56, 56, 56, 56, 52, 41, 41, 54, 56, 46, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    },
    {
      type = "tilelayer",
      name = "background",
      x = 0,
      y = 0,
      width = 25,
      height = 24,
      visible = true,
      opacity = 1,
      offsetx = 0,
      offsety = 0,
      properties = {},
      encoding = "lua",
      data = {
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 73, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 73, 73, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 73, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 73, 34, 35, 35, 19, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 35, 34, 73, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 73, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 73, 73, 35, 34, 34, 0, 0, 0, 0, 0, 0, 73, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
      }
    }
  }
}
