local menu = {}
local functions = require("core.functions")

-- get image for standart button
local button = love.graphics.newImage("images/buttons/button_bg.png")
-- and one for selected one
local button_selected = love.graphics.newImage("images/buttons/button_bg_active.png")

-- get our table for buttons functions
local tlacitka = {}

-- and this table will store actual buttons
local buttons = {}

-- number that will represent selected button
local selected = 0

-- create button with name and position
-- PLEASE: do not write code like me
-- and use czech language for variables
-- it is not good to do. Be better that me
-- NOTICE: this would be better in separate file so it looks better
function tlacitka.create(name,x,y,value)
  local value = value or nil
  -- NOTICE: selected is false we will need it later
  table.insert(buttons,{name=name,x=x,y=y,selected=false,value=value})
end

-- check buttons and mouse interaction
function tlacitka.mouse(x,y,tmp_button)
  -- it is good to have default value for parameters
  -- witht that our game will not crash on first try
  local x = x or 0
  local y = y or 0
  local tmp_button = tmp_button or false
  if not tmp_button then
    return nil
  end
  local width = button:getWidth()
  local height = button:getHeight()
  -- check if we have mouse over button if yes press it
  for key,value in pairs(buttons) do
    if functions.isInside(x,y,value.x,value.y,width, height) then
      return value.value
    end
  end
end

-- check and reset buttons
function tlacitka.check(tmp)
  -- default value again
  local tmp = tmp or 0
  local tmp_key
  for key,value in pairs(buttons) do
    -- reset and set selected buttons
    -- NOTICE: if tmp is 0 it will reset all selected to false
    if tmp == value.selected then
      selected = true
    else
      selected = false
    end
  end
end

-- load our menu items
function menu.load()
  -- NOTICE: we should store all strings in language files that we create
  -- it would solve us a lot of problems in long run
  tlacitka.create("play",100,100,"game")
  tlacitka.create("quit",100,200)
end

-- update our menu
-- main part should be check if button was selected
function menu.update(dt)
  -- get mouse position
  -- NOTICE: in this game we do not need mouse
  -- this is more like example
  local mouse_x = love.mouse.getX()
  local mouse_y = love.mouse.getY()
  -- check if we are holding down button
  local button = love.mouse.isDown(1)
  local tmp_instance = tlacitka.mouse(mouse_x,mouse_y, button)
  if tmp_instance then
    instance = tmp_instance
  end
  -- check if we should change if button is selected
  tlacitka.check(selected)
end

-- check for keys
function menu.keypressed(key,scancode)
  -- lover selected on W key
  -- NOTICE: what about up key
  if scancode == "w" then
    selected = selected + 1
  end
  -- raise selected on S key
  -- NOTICE: what about down key
  if scancode == "s" then
    selected = selected - 1
  end
  -- NOTICE: we should reset selected if it is over limit or under 0
  -- it is good to make keyboard shortcut for a lot of parts
  -- it will make debuging a lot more easy
  if scancode == "g" then
    instance = "game"
  end

end

function menu.draw()
  local width = button:getWidth()
  local height = button:getHeight()
  local font = love.graphics.getFont()
  for key,value in pairs(buttons) do
    if key == value.selected then
      love.graphics.draw(button_selected, value.x,value.y)
      love.graphics.printf(value.name, value.x, value.y, width, "center", 0, 1, 1, 0, -height/2+font:getHeight()/2)
    else
      love.graphics.draw(button, value.x,value.y)
      love.graphics.printf(value.name, value.x, value.y, width, "center", 0, 1, 1, 0, -height/2+font:getHeight()/2)
    end
  end
end

return menu
