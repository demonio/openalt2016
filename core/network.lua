local path = (...):match("(.-)[^%.]+$")
local functions = require(path.."functions")
local network = {}
local server = {}
local socket = require "socket"
local udp = socket.udp()
local tcp = socket.tcp()

-- Globals to locals
local require = require
local pairs = pairs
local love = love
local ipairs = ipairs
local print = print
local loadstring = loadstring
local tonumber = tonumber
local type = type

function network.load ()
    if love.filesystem.isFile("server.address") then
        server.address = love.filesystem.read("server.address")
    else
        server.address = "37.205.11.163"
        love.filesystem.newFile("server.address")
        love.filesystem.write("server.address", server.address)
    end
    if love.filesystem.isFile("server.tcp") then
        server.tcp = love.filesystem.read("server.tcp")
        server.udp = love.filesystem.read("server.udp")
    else
        server.tcp = 25000
        server.udp = 25001
        love.filesystem.newFile("server.tcp")
        love.filesystem.write("server.tcp", server.tcp)
        love.filesystem.newFile("server.udp")
        love.filesystem.write("server.udp", server.tcp)
    end
    --DEBUG
    --server.address = "localhost"
    udp = socket.udp()
    udp:settimeout(0)
    udp:setpeername(server.address,25001)
end

function network.connect(port)
    tcp = socket.tcp()
    tcp:connect(server.address, port);
end

function network.close ()
    tcp:close()
end

function network.test()
    network.connect(server.tcp)
    --note the newline below
    tcp:send("Connection test\n");
    tcp:settimeout(1)
    local timer = 10
    local done = false

    while timer > 0 do
        local s, status, partial = tcp:receive()
        if partial == "CONNECTION TEST" then
            timer = 0
            done = true
            break
        end
        timer = timer - 1
    end
    network.close ()
    return done
end

function network.getEntity(name)
    network.connect(server.tcp)
    --note the newline below
    tcp:send("entity get "..name.."\n");
    tcp:settimeout(1)
    local timer = 10
    local done = {}

    while timer > 0 do
        local s, status, partial = tcp:receive()
        print(s, status, partial)
        if partial then
            done = loadstring("return "..partial)()
            timer = 0
            break
        end
        timer = timer - 1
    end
    network.close ()
    return done
end

function network.update(tmp,tmp2)
  udp:send("update "..tmp.name.." "..functions.tableToDict(tmp))
  local data, msg = udp:receive()
  tmp2 = {}
  if data then
    --print(data)
    tmp2 = loadstring("return "..data)()
  end
  return tmp2
end

function network.register (player)
    network.connect (server.tcp)
    --note the newline below
    tcp:send("register "..player.id.." "..player.name.." "..player.zivoty.." "..player.level.." "..player.image.."\n")
    tcp:settimeout(1)
    local timer = 10
    local done = false

    while timer > 0 do
        local s, status, partial = tcp:receive()
        print(s, status, partial)
        if partial == "DONE" then
            timer = 0
            done = true
            break
        end
        timer = timer - 1
    end
    network.close ()
    return done
end

function network.connect_game (player,game_id)
    network.connect (server.tcp)
    --note the newline below
    tcp:send("connect "..player.id.." "..game_id.."\n")
    tcp:settimeout(1)
    local timer = 10
    local done = {}

    while timer > 0 do
        local s, status, partial = tcp:receive()
        print(s, status, partial)
        if partial then
            done = loadstring("return "..partial)()
            timer = 0
            break
        end
        timer = timer - 1
    end
    network.close ()
    return done
end

function network.get_players(id,game)
    network.connect (server.tcp)
    --note the newline below
    tcp:send("get_players "..id.." "..game.."\n")
    tcp:settimeout(1)
    local timer = 10
    local done = {}

    while timer > 0 do
        local s, status, partial = tcp:receive()
        print(s, status, partial)

        timer = timer - 1
    end
    network.close ()
    return done
end

function network.get_game(game)
    network.connect (server.tcp)
    --note the newline below
    tcp:send("get_game "..game.."\n")
    tcp:settimeout(1)
    local timer = 10
    local done = {}

    while timer > 0 do
        local s, status, partial = tcp:receive()
        print(s, status, partial)
        if partial then
            done = loadstring("return "..partial)()
            timer = 0
            break
        end
        timer = timer - 1
    end
    network.close ()
    return done
end

function network.search (number_id)
    network.connect (server.tcp)
    --note the newline below
    tcp:send("search "..number_id.."\n")
    tcp:settimeout(1)
    local timer = 10
    local done = false

    while timer > 0 do
        local s, status, partial = tcp:receive()
        print(s, status, partial)
        if tonumber(partial) ~= nil then
            timer = 0
            done = partial
            break
        end
        timer = timer - 1
    end
    network.close ()
    return done
end

function network.update(game,player)
  udp:send("update "..game.." "..player.id.." "..player.x.." "..player.y.." ")
  local data, msg = udp:receive()
  --print("test")
  local tmp2 = {}
  if data then
    --print(data)
    tmp2 = loadstring("return "..data)()
    if type(tmp2) == "table" then
      for i,v in ipairs(tmp2.players) do
        if v.id == player.id then
          player.zivoty = v.zivoty
          player.dead = v.dead
          if type(v.shoot) == "string" then
            player.shoot = loadstring("return "..v.shoot)()
          else
            player.shoot = v.shoot
          end
          tmp2.players[i]=nil
        end
      end
    end
  end
  return tmp2
end

function network.attack(attacker,defender,damage,druh,attack,time,x,y,chance)
  network.connect (server.tcp)
  --note the newline below
  tcp:send("attack "..attacker.." "..defender.." "..damage.." "..druh.." "..attack.." "..time.." "..x.." "..y.." "..chance.."\n")
  tcp:settimeout(1)
  local timer = 10
  local done = nil

  while timer > 0 do
      local s, status, partial = tcp:receive()
      --print(s, status, partial)
      if tonumber(partial) ~= nil then
          timer = 0
          done = partial
          break
      end
      timer = timer - 1
  end
  network.close ()
  return done
end


function network.score(game,team,amount)
  local amount = amount or 1
  network.connect (server.tcp)
  --note the newline below
  tcp:send("score "..game.." "..team.." "..amount.."\n")
  tcp:settimeout(1)
  local timer = 10
  local done = nil

  while timer > 0 do
      local s, status, partial = tcp:receive()
      print(s, status, partial)
      if partial == "DONE" then
          timer = 0
          done = partial
          break
      end
      timer = timer - 1
  end
  network.close ()
  return done
end

function network.kill(player)
  network.connect (server.tcp)
  --note the newline below
  tcp:send("kill "..player.."\n")
  tcp:settimeout(1)
  local timer = 10
  local done = nil

  while timer > 0 do
      local s, status, partial = tcp:receive()
      print(s, status, partial)
      if partial == "KILLED" then
          timer = 0
          done = partial
          break
      end
      timer = timer - 1
  end
  network.close ()
  return done
end

function network.dmg(player,damage)
  network.connect (server.tcp)
  --note the newline below
  tcp:send("dmg "..player.." "..damage.."\n")
  tcp:settimeout(1)
  local timer = 10
  local done = nil

  while timer > 0 do
      local s, status, partial = tcp:receive()
      print(s, status, partial)
      if partial == "DMG" then
          timer = 0
          done = partial
          break
      end
      timer = timer - 1
  end
  network.close ()
  return done
end

return network
